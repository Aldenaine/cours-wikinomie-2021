---
layout: activity
key: office_cantonal_de_l_eau
title: Titre d'activité
tags: []
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle
Actuellement, la SPDE est en réorganisation. Toute la partie système d'information est rattachée à la direction de l'OCEau (office cantonal de l'eau). Le but de cette manœuvre est d’augmenter la visibilité du SI. Cela permet aussi d'avoir plus de confidentialité, que ce soit en interne, entre les différents offices du département ou encore en externe, avec la confédération ou les différents partenaires français.
De l'autre côté, la SITG est le canal principal de mise à disposition des géodonnées, soit pour le grand public, soit pour les partenaires, communes ou encore mandataires. La SITG s'occupe d'un certain nombre de données pour le compte des communes. Un des domaines principaux géré par cet organisme est le réseau d'assainissement des eaux. Bien que ce soit la SITG qui s'occupe des données, ce sont bien les communes qui sont propriétaires des différents réseaux. Cela comprend tenir à jour le réseau, faire des travaux si nécessaire, etc. Le rôle du canton ici est simplement de contrôler et de surveiller (en plus d'avoir à disposition l'ensemble des données relatives aux eaux de Genève).

#### Règles de gouvernance

74% des données sont accessibles en Open Data, 12,5% uniquement aux partenaires SITG et 13,4% par les différents géoservices.
Ces données sont gérées par la SITG.

#### Responsables et autres acteurs

Fabrice Roth est le responsable du système d'information de l'OCEau. Il s'occupe de tout ce qui est lié aux géoinformations, géodonnées, etc. (gestion des droits d'accès, application). Il s'occupe aussi bien de la SPDE que de la SITG.
Les biologistes vont être responsables de récolter et d’analyser les données sur le terrain. De plus, et avec les SITG, ils vont discuter et déterminer quelles données vont être disponibles pour quels secteurs.


#### Résultats et utilisation
Les différentes données collectées peuvent être trouvées sous différents formats mais il est aussi possible d’afficher, via le site de la SITG, les informations sur une map. Cela nous permet de voir exactement à quel secteur appartient quelle donnée.

Il n’y a pas qu’un seul cas d’utilisation. La plupart des données sont mises à disposition de tout le monde et chacun peut avoir sa propre utilisation.

Les données sont disponibles sous différents formats. Voici la liste :

- SHAPE (ESRI)
- Geodatabase-file (ESRI)
- GML
- KML
- CSV


### Analyse des données

#### Source

Les données sont collectées directement sur le terrain, soit par des biologistes et ingénieurs, soit automatiquement par des capteurs qui envoient les données sur des serveurs.
De plus, chaque canton est responsable des données de son réseau. C’est à eux de faire les démarches nécessaires afin de récolter les données lors de travaux ou autre changement.


#### Type

On peut trouver une multitude de données différentes sur le catalogue SITG. Voici un exemple de structure avec ses attributs que l’on peut retrouver sur le site:

**- Nom**

Le champ nom correspond au nom de la donnée, on peut retrouver par exemple “ALTI_MAX”, “PENTE_MAX" ou encore “SURFACE_IMPERMEABLE" pour un cours d’eau.

**- Type**

Cet attribut représente le type de la donnée, comment elle est représentée. On peut retrouver des “String”(text) ou des doubles(nombre avec décimales), des dates et bien d’autres.

**- Taille**

C’est ici que l’on a la taille de la donnée. le nombre de lettres ou de chiffres que l’on peut retrouver. Par exemple, pour le nom du cours d’eau, on a une taille de 50.

**- Description**

Cela correspond à la description de la donnée. Pour le champ “ALTI_MAX”, cela nous informe que ce champ correspond à l’altitude maximale en mètre.

**- Origine**

Ce champ correspond à l’origine de la donnée, sa provenance.


#### Raison

Afin de déterminer si une donnée sera mise à disposition, il y a tout d’abord une discussion entre les responsables de la SITG et les biologistes à l’origine de ces données.
Ces biologistes sont des partenaires du canton.


#### Règles et dispositon

**Qui peut avoir accès aux données?**

Une première partie des données sont disponibles pour les besoins métiers externes à OCEau (pour les communes par exemple), comme pour le réseau d’assainissement. Il faut pouvoir suivre ce réseau, le cadastrer, l’entretenir afin de déterminer les coûts.
Ensuite, il y a les géodonnées appelées “grapho”. Ces données représentent le réseau hydrographique(c'est-à-dire, les cours d’eau du canton). et sont disponibles pour les métiers internes à SITG(ingénieurs).
Finalement, si la demande d’une information revient de manière récurrente, une analyse est faite et la donnée demandée peut être ajoutée afin de répondre à un intérêt général. 

**Quand les données sont-elles renouvelées?**

La fréquence de renouvellement ou de diffusion d’une donnée dépend bien entendu du domaine. Pour reprendre le réseau d’assainissement, les géodonnées correspondantes sont mises à jour quotidiennement.
Des changements ou des travaux peuvent aussi amener à un changement dans les données. 
Certaines modifications peuvent mettre 3 mois à être mises à jour sur le site qui met à disposition les données. 
Il faut savoir que ces informations sont mises à jour par des ingénieurs et des géomètres internes à OCEau. Et du moment qu’elles sont validées, elles sont intégrées au serveur métier et disponible le lendemain sur les SITG.


### Recommandations

Une des recommandations que l’on pourrait faire, c’est de les encourager à mettre un maximum de données à disposition du grand public et assurer une transparence maximale.

De plus, pour le moment, les citoyens qui veulent participer ou proposer des données, doivent passer par mail.
Il n’y a pas de plateforme où ils peuvent échanger plus facilement. L’ajout d’une section citoyenne serait intéressant.


### Bibliographie

